# aws s3 sync

Synchronise files from a bucket to another bucket using a CronJob.

## Configuration

See [Customizing the Chart Before Installing](https://helm.sh/docs/intro/using_helm/#customizing-the-chart-before-installing). To see all configurable options with detailed comments, visit the chart's [values.yaml](./values.yaml), or run these configuration commands:

```console
helm show values rsicart/aws-s3-sync
```
