# Deadman Http Daemon

Deadman Switch implementation (WatchDog alert) for Prometheus.

Default Deadman Switch implementation (WatchDog alert) for Prometheus Operator setup sends a
`POST` request periodically to default receiver /dev/null .

The objective of this project is to implement the other side of
[Deadman's Switch](https://en.wikipedia.org/wiki/Dead_man%27s_switch):

* receive `POST` requests from Prometheus in a periodic basis, which update _last seen_ ping,
* send an alert to a receiver (bypassing AlertManager) if _last seen_ ping is too old

It's important to receive `GET` requests in a regular basis, because that's the
mecanism used to check last seen ping and transition to a different state if
needed.

This server handles 2 type of HTTP requests:

* `GET` requests for healtchecks on path `/httprobe`. This path is used to periodically check if we need to send an alert.
* `POST` requests for alerts/resolves

For more details, take a look on all Deadman related modules:

*  [deadman-http-daemon](https://gitlab.com/rsicart/deadman-http-daemon)
*  [deadman-state-machine](https://gitlab.com/rsicart/deadman-state-machine)


## Configuration

The following table lists some configurable parameters of this chart and their default values.

Parameter | Description | Default
--- | --- | ---
`affinity` | node/pod affinities | `{}`
`deadman.debug` | enable verbose logging | `false`
`deadman.port` | port number where deadman http daemon listens | `8000`
`deadman.timeout` | number of seconds to wait before sending an alert to receivers | `300`
`image.pullPolicy` | container image pull policy | `IfNotPresent`
`image.repository` | container image repository | `registry.gitlab.com/rsicart/deadman-http-daemon`
`image.tag` | container image tag | `v0.2.1`
`ingress.annotations` | configure ingress annotations | `{}`
`ingress.enabled` | create an ingress to expose the service | `false`
`ingress.hosts` | list of hosts | `[ "deadman-example.local" ]`
`ingress.path` | configure ingress path | `/`
`ingress.tls` | list of secrets to use with tls | `[]`
`nodeSelector` | node labels for pod assignment | `{}`
`replicaCount` | desired number of controller pods | `1`
`resources` | pod resource requests & limits | `{}`
`service.port` | service port number | `80`
`service.type` | service type to create | `ClusterIP`
`settings` | multiline string to pass python settings to deadman http daemon | `See values.yaml`
`tolerations` | node taints to tolerate | `[]`

## Examples

### Slack

You can configure Slack as a receiver in setting.HttpPostJsonReceivers. Please check values.yaml file to for an example configuration.

After that, you also need to configure:

1. a Prometheus rule which triggers always
2. an Alertmanager receiver and a route to send an alert to this Chart's service

Prometheus example rule:

```
...
rules:
  - alert: DeadManSwitch
    expr: vector(1) == 1
    for: 5m
    labels:
      severity: critical
    annotations:
      entity: "prometheus"
      description: Prometheus DeadManSwitch is an always-firing alert. It's used as an end-to-end test of Prometheus through the Alertmanager.
```

Alertmanager example receiver:

```
config:
...
  receivers:
    - name: 'deadmanswitch'
      webhook_configs:
        - url: 'http://deadman-http-daemon.deadmanswitch.svc.cluster.local/deadman'
...
  routes:
    - match:
        alertname: DeadManSwitch
      receiver: deadmanswitch
      group_wait: 0s
      group_interval: 1m
      repeat_interval: 50s
...
```
